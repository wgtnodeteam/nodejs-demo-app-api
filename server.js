var express = require('express'),
	app = express(),
	port = process.env.PORT || 3000,
	mongoose = require('mongoose'),
	cors = require('cors'),
	bodyParser = require('body-parser'),
	jwt = require('jsonwebtoken'),  // used to create, sign, and verify tokens
	config = require('./config');   // get our config file

var indexRoutes = require('./api/routes/index'),
    userRoutes = require('./api/routes/user'),
    authenticate = require('./api/routes/authenticate');

mongoose.Promise = global.Promise;
mongoose.connect(config.database, {useMongoClient: true});
app.set('superSecret', config.secret);  //secret variable

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

app.use('/user', userRoutes);
app.use('/authenticate', authenticate);
app.use('/', indexRoutes);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);
console.log('API server started on port: '+port);
