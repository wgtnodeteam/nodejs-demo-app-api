var express = require('express');
var router = express.Router();
var userController = require('../controllers/user');

router.get('/', userController.getUsers)
router.post('/', userController.create_users);

module.exports = router;