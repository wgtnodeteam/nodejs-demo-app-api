var express = require('express');
var router = express.Router();
var authenticateUserController = require('../controllers/authenticateUser');

router.post('/', authenticateUserController.getCurrentUser)

module.exports = router;