var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	create_dt: {
		type: Date,
		default: Date.now
	},
	update_dt: {
		type: Date
	}
});

module.exports = mongoose.model('User', UserSchema);